/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.shopping.services;

import com.mycompany.shopping.models.ProductModel;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author java
 */
@Named("shopService")
@ApplicationScoped
public class ShopService {

    private List<ProductModel> products;

    @PostConstruct
    public void init() {
        products = new ArrayList<>();
        products.add(ProductModel.builder().id(UUID.randomUUID().toString()).name("PC").description("Personal Computer").build());
        products.add(ProductModel.builder().id(UUID.randomUUID().toString()).name("Laptop").description("Portable Computer").build());
        products.add(ProductModel.builder().id(UUID.randomUUID().toString()).name("Server").description("Server Machine").build());
        products.add(ProductModel.builder().id(UUID.randomUUID().toString()).name("Switch").description("Switch Network Device").build());
        products.add(ProductModel.builder().id(UUID.randomUUID().toString()).name("USB Hub").description("Allow multiple device to connect").build());
        products.add(ProductModel.builder().id(UUID.randomUUID().toString()).name("USB Web Cam").description("Web Camera with USB connector").build());
        products.add(ProductModel.builder().id(UUID.randomUUID().toString()).name("Monitor").description("DELL 24 inches").build());
        products.add(ProductModel.builder().id(UUID.randomUUID().toString()).name("Mouse").description("Delux Gaming").build());
        products.add(ProductModel.builder().id(UUID.randomUUID().toString()).name("Mainframe").description("IBM Mainframe").build());
    }

    public List<ProductModel> getProducts() {
        return products;
    }
}
