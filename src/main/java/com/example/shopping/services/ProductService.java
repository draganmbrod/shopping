/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.shopping.services;

import com.example.shopping.hibernate.dao.ProductDao;
import com.example.shopping.hibernate.entity.Product;
import java.util.List;

/**
 *
 * @author vasilija
 */
public class ProductService {

    private static ProductDao productDao;

    public ProductService() {
        productDao = new ProductDao();
    }

    public void persist(Product entity) {
        productDao.openCurrentSessionWithTransaction();
        productDao.persist(entity);
        productDao.closeCurrentSessionWithTransaction();
    }

    public void update(Product entity) {
        productDao.openCurrentSessionWithTransaction();
        productDao.update(entity);
        productDao.closeCurrentSessionWithTransaction();
    }

    public Product findById(String id) {
        productDao.openCurrentSessionWithTransaction();
        Product product = productDao.findBy(id);
        productDao.closeCurrentSessionWithTransaction();
        return product;
    }

    public List<Product> findAll() {
        productDao.openCurrentSession();
        List<Product> products = productDao.findAll();
        return products;
    }
    
    public void deleteAll(){
    productDao.openCurrentSessionWithTransaction();
    productDao.deleteAll();
    productDao.closeCurrentSession();
    }
    
    public ProductDao productDao(){
    return productDao;
    }
}
