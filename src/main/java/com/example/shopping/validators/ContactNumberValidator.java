/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.shopping.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * Implementation for {@link ContactNumber}
 *
 * @author Vasilija Uzunova
 */
public class ContactNumberValidator implements ConstraintValidator<ContactNumber, String> {

    @Override
    public void initialize(ContactNumber contactNumber) {
    }

    @Override
    public boolean isValid(String contactField, ConstraintValidatorContext cxt) {
        return StringUtils.isNotBlank(contactField) && NumberUtils.isDigits(contactField)
                && StringUtils.length(contactField) > 8 && StringUtils.length(contactField) < 14;
    }

}
