package com.example.shopping.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 * @author Vasilija Uzunova
 */
@Documented
@Constraint(validatedBy = ContactNumberValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ContactNumber {
    String message() default "Invalid contact phone number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}

