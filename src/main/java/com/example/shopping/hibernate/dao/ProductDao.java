/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.shopping.hibernate.dao;

import com.example.shopping.hibernate.entity.Product;
import com.example.shopping.util.HibernateUtil;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author vasilija
 */
@Setter
@Getter
public class ProductDao implements ProductDaoInterface<Product, String> {
    
    private Session currentSession;
    
    private Transaction currentTransaction;
    
    public Session openCurrentSession(){
    currentSession = HibernateUtil.getSessionFactory().openSession();
    return currentSession;
    }
    
    public Session openCurrentSessionWithTransaction(){
    currentSession = HibernateUtil.getSessionFactory().openSession();
    currentTransaction = currentSession.beginTransaction();
    return currentSession;
    }
    
    public void closeCurrentSession(){
    currentSession.close();
    }
    
    public void closeCurrentSessionWithTransaction(){
    currentTransaction.commit();
    currentSession.close();
    }

    @Override
    public void persist(Product entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Product entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Product findBy(String id) {
        Product product = (Product) getCurrentSession().get(Product.class, id);
        return product;
    }

    @Override
    public void delete(Product entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<Product> findAll() {
        List<Product> products = (List<Product>) getCurrentSession().createQuery("from Product").list();
        return products;
    }

    @Override
    public void deleteAll() {
        List<Product> entityList = findAll();
        entityList.stream().forEach(entity -> delete(entity));
    }
    
}
