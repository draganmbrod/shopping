/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shopping.models;

import com.mycompany.shopping.beans.products.TabProduct;
import lombok.Builder;
import lombok.Getter;

/**
 *
 * @author vasilija
 */
@Builder
@Getter
public class TabInfo {
    private final String nameOfTab;
    private final TabProduct nameOfBean;
}
