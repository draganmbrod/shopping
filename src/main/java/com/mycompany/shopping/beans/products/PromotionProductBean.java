/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shopping.beans.products;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author vasilija
 */
@Named("promotion")
@Getter
@Setter
@SessionScoped
public class PromotionProductBean extends TabProduct implements Serializable {

    @PostConstruct
    public void setTabName() {
        tabName = "Promotion";
    }
}
