/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shopping.beans.products;

import com.example.shopping.hibernate.entity.Product;
import com.example.shopping.services.ProductService;
import com.mycompany.shopping.models.TabInfo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author vasilija
 */
@Named("tabBean")
@Getter
@Setter
@SessionScoped
public class DisplayTabBean implements Serializable {

    private List<TabInfo> tabs;
    
    private List<Product> products;

    @Inject
    private DeliveryProductBean delivery;

    @Inject
    private ProcProductBean procurement;

    @Inject
    private PromotionProductBean promotion;

    private List<DeliveryProductBean> deliveryProducts = new ArrayList<>();
    private List<ProcProductBean> procurementProducts = new ArrayList<>();
    private List<PromotionProductBean> promotionProducts = new ArrayList<>();

    @PostConstruct
    public void init() {
        ProductService pService = new ProductService();
        tabs = Arrays.asList(TabInfo.builder().nameOfTab("Promocija").nameOfBean(promotion).build(),
                TabInfo.builder().nameOfTab("Nabavka").nameOfBean(procurement).build(),
                TabInfo.builder().nameOfTab("Dostava").nameOfBean(delivery).build());
        products = pService.findAll();
    }

    public void insertProduct(TabProduct bean) {
        if (bean == null) return;
        
        if (bean instanceof DeliveryProductBean) {
            deliveryProducts.add((DeliveryProductBean) bean);
        } else if (bean instanceof ProcProductBean) {
            procurementProducts.add((ProcProductBean) bean);
        } else if (bean instanceof PromotionProductBean) {
            promotionProducts.add((PromotionProductBean) bean);
        }
        
        System.out.println("added: " + bean);
    }
}
