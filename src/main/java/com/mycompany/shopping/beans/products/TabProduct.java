/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shopping.beans.products;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author vasilija
 */
@Getter
@Setter
@ToString
public class TabProduct {

    protected String name;
    protected String description;
    protected String tabName;

}
