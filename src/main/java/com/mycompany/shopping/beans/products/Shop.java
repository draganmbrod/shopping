/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shopping.beans.products;

import com.example.shopping.hibernate.entity.Product;
import com.example.shopping.services.ProductService;
import com.example.shopping.services.ShopService;
import com.mycompany.shopping.models.ProductModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author java
 */
@Named("shop")
@SessionScoped
@Getter
@Setter
public class Shop implements Serializable {

    private ProductModel selectedProduct;
    private List<ProductModel> productsList;

    @Inject
    private ShopService service;

    @PostConstruct
    public void init() {
        productsList = service.getProducts();
    }    
    public String saveProduct(){
        ProductService ps = new ProductService();
        Product productToSave = Product.builder()
                                .id(selectedProduct.getId())
                                .name(selectedProduct.getName())
                                .description(selectedProduct.getDescription())
                                .build();
        ps.persist(productToSave);
        return "tabsdisplay";
    }
    

    public List<String> completeText(String query) {
        List<String> results = new ArrayList<String>();

        for (ProductModel product : productsList) {
            if (product.getName().startsWith(query)) {
                results.add(product.getName());
            }
        }

        return results;
    }

}
