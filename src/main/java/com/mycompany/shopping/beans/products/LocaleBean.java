package com.mycompany.shopping.beans.products;

import java.io.Serializable;
import java.util.Locale;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Named("locale")
@ApplicationScoped
@Getter
@Setter
public class LocaleBean implements Serializable {

    private Locale currLocale = new Locale("mk", "MK");
    private String code;

    public void langChanged () {
        FacesContext context = FacesContext.getCurrentInstance();
        String country = StringUtils.split(code, "_")[1];
        String language = StringUtils.split(code, "_")[0];
        currLocale = new Locale(language, country);
        context.getViewRoot().setLocale(currLocale);
    }
}