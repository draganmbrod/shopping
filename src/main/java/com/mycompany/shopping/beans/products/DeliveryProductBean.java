/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shopping.beans.products;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author vasilija
 */
@Named("delivery")
@Getter
@Setter
@SessionScoped
public class DeliveryProductBean extends TabProduct implements Serializable {

    @PostConstruct
    public void setTabName() {
        tabName = "Delivery";
    }

}
