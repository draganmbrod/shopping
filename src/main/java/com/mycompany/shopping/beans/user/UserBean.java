package com.mycompany.shopping.beans.user;

import com.example.shopping.validators.ContactNumber;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Vasilija Uzunova
 */
@Named("userBean")
@SessionScoped
@Getter
@Setter
public class UserBean implements Serializable {
    
    @NonNull
    private String name;
    
    @ContactNumber
    private String contactNumber;
}
