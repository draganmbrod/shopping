/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tests;

import java.io.IOException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.Test;

/**
 *
 * @author java
 */
public class LoginPageTest {

    public static void main(String[] args) {
        try {
            testLogin();
        } catch (SecurityException ex) {
            Logger.getLogger(LoginPageTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LoginPageTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(LoginPageTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public static void testLogin() throws SecurityException, IOException, InterruptedException {

        System.setProperty("webdriver.gecko.driver", "C:\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver();

        String baseUrl = "http://localhost:7001/Shopping/";
        String actualTitle = "";

        driver.get(baseUrl);

        actualTitle = driver.getTitle();

        System.out.println("Test Passed! - Title is : " + actualTitle);

        Thread.sleep(2000);
        WebElement name = driver.findElement(By.id("loginForm:name"));
        name.sendKeys("Customer");

        Thread.sleep(2000);
        WebElement contactNumber = driver.findElement(By.id("loginForm:contactNumber"));
        contactNumber.sendKeys("123456");

        Thread.sleep(2000);
        WebElement loginButton = driver.findElement(By.id("loginForm:loginButton"));
        loginButton.click();

        Thread.sleep(2000);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement text2 = wait.until(visibilityOfElementLocated(By.id("loginForm:contactNumber")));
        text2.clear();
        text2.sendKeys("123456789");

        Thread.sleep(2000);
        loginButton.click();

        Thread.sleep(2000);
        //close Firefox
        driver.close();

    }
}
